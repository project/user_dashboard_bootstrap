/**
 * @file
 * User dashboard Bootstrap behaviors.
 */
(function ($, drupalSettings, once) {

  'use strict';

  Drupal.behaviors.userDashboardBootstrapLayout = {
    attach(context, settings) {
      let options = {
        row: $('.grid-stack').length,
        dragOut: true,
        float: false,
        cellHeight: 'auto',
        cellHeightThrottle: 100,
        acceptWidgets: true,
        removable: true,
        animate: true,
        column: 6,
        columnOpts: {
          columnWidth: 100,
        },
        staticGrid: true,
      }
      let grids = GridStack.initAll(options);
      GridStack.setupDragIn('.list-block .grid-stack-item', { appendTo: '.grid-stack', });
      $(once('grid-stack', '.grid-stack')).each(function () {
        let id = $(this).attr('id');
        grids[id] = GridStack.init(options, document.getElementById(id));
        console.log(document.getElementById(id))
        let roleData = $(this).closest('.js-form-type-textarea').find('textarea').val();
        if (roleData !== '') {
          grids[id].load(JSON.parse(roleData));
        }
      });
    }
  };

})(jQuery, drupalSettings, once);
