(function ($, drupalSettings, once) {
  function addEvents(grid, id) {
    let g = (id !== undefined ? 'grid' + id + ' ' : '');
    grid.on('dropped', function(event, previousNode, newNode) {
      let data = newNode.el.dataset;
      let urlBlockContent = drupalSettings.dashboard.blockContent + '/' + data.delta;
      $.get(urlBlockContent, function (data) {
        newNode.el.classList.remove('col-auto', 'border', 'rounded', 'me-2', 'mb-2');
        const itemStack = newNode.el.querySelector('.grid-stack-item-content');
        itemStack.innerHTML = data;
        $(itemStack).find('.hidden').removeClass('hidden');
        $(itemStack).find('.btn-close').on('click', function () {
          const el = this.closest('.block');
          if (el) {
            el.remove();
            grid.removeWidget(el);
          }
        });
      });
    })
    .on('removed resizestop', function(event, previousNode, newNode) {
      if(!$('.btn-save').length) {
        $('#load-disabled-blocks').click();
      }
    });
  }
  Drupal.behaviors.user_dashboard = {
    attach: function (context, settings) {
      let options = {
        float: false,
        cellHeight: 100,
        cellHeightThrottle: 100,
        acceptWidgets: true,
        removable: true,
        animate: true,
        column: 12,
        columnOpts: {
          breakpointForWindow: true,
          breakpoints: [{w:700, c:1},{w:850, c:3},{w:950, c:6},{w:1100, c:8}]
        },
        staticGrid: false
      };
      once('grid-stack', '.grid-stack').forEach(function (element) {
        const grid = GridStack.init(options);
        addEvents(grid);
        GridStack.setupDragIn('.list-block .grid-stack-item', { appendTo: '.grid-stack', });
      });
      $(once('load-blocks','.customize #load-disabled-blocks')).click(function (e) {
        const grid = GridStack.init(options);
        e.preventDefault();
        //send the blocks to our default blocks callback
        $('#dashboard .canvas .canvas-content').removeClass('hidden');
        $('#dashboard .btn-close.hidden').removeClass('hidden');
        $('.layout-row').removeClass('d-none');
        function close() {
          $('#dashboard .btn-close').on('click', function () {
            let bid = $(this).data('bid');
            let that = $(this);
            $.post(drupalSettings.dashboard.updatePath, {action: 'delete', bid: bid})
              .done(function (data) {
                that.closest('.card').remove();
              })
              .fail(function () {
                Drupal.t("An error occurred during the update of the entity. Please consult the watchdog.");
              });
          });
        }
        function save() {
          $('.btn-save').on('click',function (e) {
            e.preventDefault();
            options = grid?.save(true, false);
            options.forEach(function(item) {
              delete item.content;
            });
            $.post(drupalSettings.dashboard.drawer, {gridStack: JSON.stringify(options)})
              .done(function (data) {
                window.location.href = $('.btn-save').attr('href');
              })
              .fail(function () {
                Drupal.t("An error occurred during the update of the entity. Please consult the watchdog.");
              });
          });
        }
        $.get(drupalSettings.dashboard.customize, function (data) {
          $('#dashboard .canvas .canvas-content .disabled-blocks').html(data);
          close();
          save();
          GridStack.setupDragIn('.list-block .grid-stack-item', {
            appendTo: '.grid-stack',
          });
        });
      });
    }
  };
})(jQuery, drupalSettings, once);
