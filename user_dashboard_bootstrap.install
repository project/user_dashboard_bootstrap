<?php

/**
 * @file
 * Install, update & uninstall functions for User dashboard Bootstrap module.
 */

use Drupal\Core\Database\Database;

/**
 * Implements hook_install().
 */
function user_dashboard_bootstrap_install() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_uninstall().
 */
function user_dashboard_bootstrap_uninstall() {
  \Drupal::messenger()->addStatus(__FUNCTION__);
}

/**
 * Implements hook_schema().
 */
function user_dashboard_bootstrap_schema() {
  $schema['user_dashboard_block'] = [
    'description' => 'Table description.',
    'fields' => [
      'bid' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique block ID.',
      ],
      'delta' => [
        'description' => 'Unique ID for block within a module.',
        'type' => 'varchar',
        'length' => '255',
        'not null' => TRUE,
        'default' => '0',
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid.',
      ],
      'status' => [
        'description' => 'Block enabled status. (1 = enabled, 0 = disabled)',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'weight' => [
        'description' => 'Block weight within region.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'region' => [
        'description' => 'Theme region within which the block is set.',
        'type' => 'varchar',
        'length' => '64',
        'not null' => TRUE,
        'default' => '',
      ],
      'custom' => [
        'description' => 'Flag to indicate how users may control visibility of the block. (0 = Users cannot control, 1 = On by default, but can be hidden, 2 = Hidden by default, but can be shown)',
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE,
        'default' => 1,
      ],
      'position' => [
        'description' => 'Position of block (stored as JSON string)',
        'type' => 'text',
        'not null' => FALSE,
      ],
    ],
    'primary key' => ['bid'],
    'indexes' => [
      'uid' => ['uid'],
    ],
  ];

  return $schema;
}

/**
 * Adds 'position' column to 'user_dashboard_block' table.
 */
function user_dashboard_bootstrap_update_8001() {
  // Define the new field 'position'.
  $field = [
    'description' => 'Position of block (stored as JSON string)',
    'type' => 'text',
    'not null' => FALSE,
    'default' => '',
  ];
  // Add the field position to the 'user_dashboard_block' table.
  $schema = Database::getConnection()->schema();
  if (!$schema->fieldExists('user_dashboard_block', 'position')) {
    $schema->addField('user_dashboard_block', 'position', $field);
  }
}
