# User dashboard

Provide an individual dashboard for each user.

- Users can access their dashboards at the /user/dashboard
(or /user/[uid]/dashboard) page, User can drag & drop block into dashboard.
- Administrators can configure which blocks can be used on
the user dashboard via the settings form at
/admin/dashboard/user_dashboard/settings.
- Administrators can configure user dashboard default by role
- Send email report daily
- Support gridStack

Tested on theme [boostrap 5 admin](https://www.drupal.org/project/bootstrap5_admin) If you're using the claro theme, you won't see some features

For a full description of the module, visit the
[project page](https://www.drupal.org/project/user_dashboard_bootstrap).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/user_dashboard_bootstrap).


## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Goto "user dashboard" settings (/admin/dashboard/user_dashboard/settings)
to allows blocks for user. You can select option Send email daily, use GridStack
- When you select the daily email send option, it'll run cron every day,
render the dashboard page and send it to the user's email address.
- The option "use the gridStack" will help to position / resize the blocks

## Maintainers

- NGUYEN Bao - [lazzyvn](https://www.drupal.org/u/lazzyvn)
