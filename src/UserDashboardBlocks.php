<?php

namespace Drupal\user_dashboard_bootstrap;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\Context\LazyContextRepository;
use Drupal\Core\Plugin\PluginFormFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * User dashboard Service.
 */
class UserDashboardBlocks {
  use StringTranslationTrait;

  /**
   * DraggableDashboardController constructor.
   *
   * @inheritDoc
   */
  public function __construct(protected BlockManagerInterface $blockManager, protected LazyContextRepository $contextRepository, protected PluginFormFactoryInterface $pluginFormFactory, protected EntityTypeManager $entityManager, protected Connection $database, protected ConfigFactoryInterface $configFactory) {
  }

  /**
   * Get blocks definition.
   *
   * @inheritDoc
   */
  public function getDefinitions() {
    // Only add blocks which work without any available context.
    $definitions = $this->blockManager->getFilteredDefinitions('block_ui', $this->contextRepository->getAvailableContexts());

    // Order by category, and then by admin label.
    $definitions = $this->blockManager->getSortedDefinitions($definitions);
    // Filter out definitions that are not intended to be placed by the UI.
    return array_filter($definitions, function (array $definition) {
      return empty($definition['_block_ui_hidden']);
    });
  }

  /**
   * Shows a list of blocks that can be added to a theme's layout.
   *
   * @inheritDoc
   */
  public function listBlocks() {
    $definitions = $this->getDefinitions();
    $blocks = [];
    foreach ($definitions as $plugin_id => $plugin_definition) {
      $blocks[$plugin_id] = $plugin_definition['admin_label'] . ' - ' . $plugin_definition['category'];
    }
    asort($blocks);
    return $blocks;
  }

  /**
   * Get Regions.
   *
   * @inheritDoc
   */
  public function getRegions() {
    return [
      'main' => [
        'user_dashboard_main' => [
          'name' => 'Dashboard (main)',
          'width' => 8,
        ],
        'user_dashboard_sidebar' => [
          'name' => 'Dashboard (sidebar)',
          'width' => 4,
        ],
      ],
      'second' => [
        'user_dashboard_column1' => [
          'name' => 'Dashboard (column1)',
          'width' => 8,
        ],
        'user_dashboard_column2' => [
          'name' => 'Dashboard (column2)',
          'width' => 4,
        ],
      ],
      'simple' => [
        'user_dashboard_column3' => [
          'name' => 'Dashboard (column3)',
          'width' => 12,
        ],
      ],
      'footer' => [
        'user_dashboard_footer' => [
          'name' => 'Dashboard (footer)',
          'width' => 12,
        ],
      ],
    ];
  }

  /**
   * Get all user blocks shown in dashboard.
   *
   * {@inheritDoc}
   */
  public function getUserdashboardBlocks($user) {
    $query = $this->database->select('user_dashboard_block', 'b');
    $query->fields('b')->condition('uid', $user)
      ->orderBy('weight', 'ASC');
    return $query->execute()->fetchAllAssoc('delta', \PDO::FETCH_ASSOC);
  }

  /**
   * Get user block in region.
   *
   * @inheritDoc
   */
  public function getUserdashboardRegionsBlocks($user, $blockAvailable = []) {
    $regionsBlock = [];
    $blocks = !empty($blockAvailable) ? $blockAvailable : $this->getUserdashboardBlocks($user);
    if (!empty($blocks)) {
      foreach ($blocks as $block) {
        $region = $block['region'];
        $delta = $block['delta'];
        $regionsBlock[$region][$delta] = $this->renderUserdashboardBlock($user, $delta);
        if (empty($regionsBlock[$region][$delta])) {
          continue;
        }
        if ($block['custom'] == 2) {
          $regionsBlock[$region][$delta]['#collapse'] = TRUE;
        }
        $regionsBlock[$region][$delta]['#bid'] = $block['bid'];
        $regionsBlock[$region][$delta]['#weight'] = $block['weight'];
      }
    }
    $regions = $this->getRegions();
    foreach ($regions as $index => $row) {
      foreach ($row as $name => $region) {
        if (!empty($regionsBlock[$name])) {
          $regions[$index][$name]['block'] = $regionsBlock[$name];
        }
      }
    }
    return $regions;
  }

  /**
   * Render userdashboard block.
   *
   * @inheritDoc
   */
  public function renderUserdashboardBlock($user, $delta) {
    if (is_numeric($user)) {
      $user = $this->entityManager->getStorage('user')->load($user);
    }
    $blocks = $this->getDefinitions();

    if (empty($blocks[$delta])) {
      $this->database->delete('user_dashboard_block')
        ->condition('uid', $user->id())
        ->condition('delta', $delta)
        ->execute();
      return FALSE;
    }

    $lockBlock = $this->configFactory->get('user_dashboard_bootstrap.lock')->get('lock_block');
    $arrBlock = $lockBlock ?? [];

    $config = ['label' => $blocks[$delta]['admin_label']];
    $plugin_block = $this->blockManager->createInstance($delta, $config);
    $render = '';
    if ($plugin_block->access($user)) {
      $render = $plugin_block->build();
      if (!empty($render["#view"]) && empty($render["#view"]->result)) {
        return FALSE;
      }
      if (empty($render)) {
        return FALSE;
      }
    }
    $query = $this->database->select('user_dashboard_block', 'b')
      ->fields('b', ['position'])
      ->condition('delta', $delta)
      ->condition('uid', $user->id());
    $position = $query->execute()->fetchField();
    if (!empty($position)) {
      $positionBlock = json_decode($position, TRUE);
      $x = $positionBlock['x'];
      $y = $positionBlock['y'];
      $w = $positionBlock['w'];
      $h = $positionBlock['h'] ?? 0;
    }
    $config = $this->configFactory->get('user_dashboard_bootstrap.settings');
    $gridStack = $config->get('gridstack');
    return [
      '#theme' => 'user_dashboard',
      '#id' => str_replace(':', '-', $delta),
      '#delta' => $delta,
      '#collapse' => FALSE,
      '#title' => $blocks[$delta]['admin_label'] ?? $this->t('Missing block'),
      '#content' => $render,
      '#weight' => 0,
      '#gridStack' => $gridStack,
      '#lock' => in_array($delta, $arrBlock),
      '#h' => $h ?? 0,
      '#x' => $x ?? 0,
      '#y' => $y ?? 0,
      '#w' => $w ?? 0,
    ];
  }

}
