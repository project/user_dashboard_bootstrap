<?php

namespace Drupal\user_dashboard_bootstrap\Plugin\QueueWorker;

/**
 * @file
 * Contains Drupal\user_dashboard_bootstrap\Plugin\QueueWorker\UserDashboardCron.php.
 */

use Drupal\Core\Config\Config;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\Attribute\QueueWorker;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountSwitcher;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\PermissionHandlerInterface;
use Drupal\user\UserInterface;
use Drupal\user_dashboard_bootstrap\UserDashboardBlocks;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * A queue worker that processes send email daily report on CRON run.
 */
#[QueueWorker(
  id: 'cron_user_dashboard_email',
  title: new TranslatableMarkup('Cron user dashboard sends emails daily'),
  cron: ['time' => 300]
)]
class UserDashboardCron extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * User storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private EntityStorageInterface $userStorage;

  /**
   * Constructs a new OrphanPurger instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Session\AccountSwitcher $accountSwitcher
   *   Switch user.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\user_dashboard_bootstrap\UserDashboardBlocks $userDashboardBlocks
   *   User dashboard blocks service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   * @param \Drupal\Core\Config\Config $config
   *   Config system.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Session\SessionManagerInterface $sessionManager
   *   The session manager.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   * @param \Drupal\user\PermissionHandlerInterface $permissionHandler
   *   The permission handler.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected EntityTypeManagerInterface $entityTypeManager, protected AccountSwitcher $accountSwitcher, protected MailManagerInterface $mailManager, protected UserDashboardBlocks $userDashboardBlocks, protected RendererInterface $renderer, protected Config $config, protected AccountInterface $currentUser, protected ModuleHandlerInterface $moduleHandler, protected SessionManagerInterface $sessionManager, protected SessionInterface $session, protected PermissionHandlerInterface $permissionHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userStorage = $this->entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('account_switcher'),
      $container->get('plugin.manager.mail'),
      $container->get('user_dashboard_bootstrap.blocks'),
      $container->get('renderer'),
      $container->get('config.factory')->get('system.site'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('session_manager'),
      $container->get('session'),
      $container->get('user.permissions'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!empty($data) && !empty($data['#uid'])) {
      $key = 'email_dailly';
      if (empty($data['key'])) {
        $data['key'] = $key;
      }
      if ($user = $this->userStorage->load($data['#uid'])) {
        $this->switchUser($user);
        $data['#regions'] = $this->userDashboardBlocks->getUserdashboardRegionsBlocks($data['#uid'], $data['blocks']);
        unset($data['blocks']);
        $template = $this->emailRenderer($data);
        $this->mailManager->getInstance([
          'module' => 'user_dashboard_bootstrap',
          'key' => $key,
        ])->mail($template);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function emailRenderer($context = []) {
    global $base_url;
    $id = $context['key'] ?? 'email_dailly';
    unset($context['key']);
    if (empty($context)) {
      return FALSE;
    }
    $body = $this->renderer->renderInIsolation($context);
    $body = str_replace(['href="/', 'src="/'], [
      'href="' . $base_url . base_path(),
      'src="' . $base_url . base_path(),
    ], $body);
    return [
      'id' => $id,
      'headers' => [
        'Content-type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
        'Reply-to' => $context['#site_name'] . '<' . $context['#site_email'] . '>',
        'Content-Transfer-Encoding' => '8Bit',
        'MIME-Version' => '1.0',
      ],
      'to' => $context['#username'] . '<' . $context['#email'] . '>',
      'subject' => $context['#subject'] ?? $context['#site_name'],
      'body' => $body,
      'Return-Path' => $this->config->get('mail'),
    ];
  }

  /**
   * Logs out current user and logs in as pointed user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity to switch to.
   *
   * @return \Drupal\user\UserInterface
   *   The previous user entity.
   *
   * @see \Drupal\Core\Session\SessionHandler::write()
   * @see \Drupal\user\Authentication\Provider\Cookie::getUserFromSession()
   */
  protected function switchUser(UserInterface $user) {
    // Regenerate the session ID to prevent against session fixation attacks.
    $currentUser = $this->currentUser;
    $this->sessionManager->regenerate();
    $this->accountSwitcher->switchTo($user);
    $this->session->set('uid', $user->id());
    return $currentUser;
  }

}
