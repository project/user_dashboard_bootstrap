<?php

namespace Drupal\user_dashboard_bootstrap\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user_dashboard_bootstrap\UserDashboardBlocks;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure User dashboard Bootstrap settings for this site.
 */
class UserDashboardLayout extends ConfigFormBase {

  /**
   * Name of the form.
   *
   * @var string
   */
  public static string $formName = 'user_dashboard_bootstrap.layout';
  /**
   * Roles list.
   *
   * @var array
   */
  protected mixed $roles;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The type config manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\user_dashboard_bootstrap\UserDashboardBlocks $userDashboardBlocks
   *   The service of user dashboard.
   * @param \Drupal\Core\Database\Connection $database
   *   Service database.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected TypedConfigManagerInterface $typedConfigManager, protected EntityTypeManagerInterface $entityTypeManager, protected UserDashboardBlocks $userDashboardBlocks, protected Connection $database) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->roles = $this->entityTypeManager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('user_dashboard_bootstrap.blocks'),
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'user_dashboard_layout_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [self::$formName];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $blocks = $this->userDashboardBlocks->listBlocks();
    $blocksAvailable = $this->config('user_dashboard_bootstrap.settings')
      ->get('user_dashboard_available_blocks');
    if (!empty($blocksAvailable)) {
      foreach ($blocks as $delta => $block) {
        if (empty($blocksAvailable[$delta])) {
          unset($blocks[$delta]);
        }
      }
    }
    foreach ($blocks as $delta => $block) {
      $blocks[$delta] = ['admin_label' => $block];
    }
    $theme = 'user_dashboard_gridstack_blocks';
    $form['blocks'] = [
      '#theme' => $theme,
      '#blocks' => $blocks,
    ];

    $roles = $this->roles->loadMultiple();
    // Remove role anonymous.
    unset($roles['anonymous']);
    foreach ($roles as $roleName => $role) {
      $form[$roleName] = [
        '#type' => 'textarea',
        '#title' => $role->label(),
        '#default_value' => $this->config('user_dashboard_bootstrap.layout')->get($roleName),
        '#attributes' => [
          'class' => ['js-hide'],
        ],
        '#description' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'class' => 'grid-stack',
            'id' => $roleName,
          ],
        ],
      ];
    }
    $form['#attached']['library'] = ['user_dashboard_bootstrap/user_dashboard_layout'];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('user_dashboard_bootstrap.layout')
      ->set('user_dashboard_available_blocks', $form_state->getValue('user_dashboard_available_blocks'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
