<?php

namespace Drupal\user_dashboard_bootstrap\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user_dashboard_bootstrap\UserDashboardBlocks;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure User dashboard Bootstrap settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Name of the config.
   *
   * @var string
   */
  public static $configName = 'user_dashboard_bootstrap.settings';

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The Type config manager service.
   * @param \Drupal\user_dashboard_bootstrap\UserDashboardBlocks $userDashboardBlocks
   *   The service of user dashboard.
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected TypedConfigManagerInterface $typedConfigManager, protected UserDashboardBlocks $userDashboardBlocks) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('user_dashboard_bootstrap.blocks'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_dashboard_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::$configName];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::$configName);
    $blocks = $this->userDashboardBlocks->listBlocks();
    $valuesBlock = $config->get('user_dashboard_available_blocks');

    $form['link'] = [
      '#type' => 'link',
      '#title' => $this->t('Configuration user dashboard block available by role'),
      '#url' => Url::fromRoute('user_dashboard_bootstrap.set_default'),
      '#attributes' => ['class' => ['btn', 'btn-success']],
    ];
    $form['layout'] = [
      '#type' => 'link',
      '#title' => $this->t('Set user dashboard layout by role'),
      '#url' => Url::fromRoute('user_dashboard_bootstrap.user_role_layout'),
      '#attributes' => ['class' => ['btn', 'btn-warning', 'ms-2']],
    ];
    $form['lock'] = [
      '#type' => 'link',
      '#title' => $this->t('Lock user layout'),
      '#url' => Url::fromRoute('user_dashboard_bootstrap.user_lock_block'),
      '#attributes' => ['class' => ['btn', 'btn-info', 'ms-2']],
    ];
    $form['user_dashboard_available_blocks'] = [
      '#title' => $this->t('Available blocks for user'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $blocks,
      '#default_value' => $valuesBlock ?? [],
      '#description' => $this->t('Choose blocks that can be used on the user dashboard pages.'),
    ];
    $form['cron_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send email daily.'),
      '#default_value' => $config->get('cron_email') ?? FALSE,
      '#description' => $this->t('Sending Dashboard as email reports.'),
    ];
    $form['user_dashboard_cron_email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Send email subject.'),
      '#default_value' => $config->get('user_dashboard_cron_email_subject') ?? $this->t('Daily report'),
      '#states' => [
        'visible' => [
          ':input[name^="cron_email"]' => [['checked' => TRUE]],
        ],
      ],
    ];
    $form['cron_hour'] = [
      '#type' => 'select',
      '#options' => range(0, 23),
      '#title' => $this->t('Hour to send email.'),
      '#default_value' => $config->get('cron_hour') ?? 0,
      "#empty_option" => $this->t('- Select -'),
      '#states' => [
        'visible' => [
          ':input[name^="cron_email"]' => [['checked' => TRUE]],
        ],
      ],
    ];

    $form['user_dashboard_cron_email_block'] = [
      '#title' => $this->t('Allow block daily email sending'),
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $blocks,
      '#default_value' => $config->get('user_dashboard_cron_email_block') ?? [],
      '#description' => $this->t('If nothing is selected, it will take the entire user dashboard block to send the mail. If a block is selected Only the selected block can be displayed for the email report.'),
      '#states' => [
        'visible' => [
          ':input[name^="cron_email"]' => [['checked' => TRUE]],
        ],
      ],
    ];
    $form['gridstack'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use gridstack'),
      '#default_value' => $config->get('gridstack') ?? FALSE,
      '#description' => $this->t('It will use gridstack.js for dragdrop resize'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::$configName)
      ->set('user_dashboard_available_blocks', array_diff($form_state->getValue('user_dashboard_available_blocks'), [0]))
      ->set('user_dashboard_cron_email_block', array_diff($form_state->getValue('user_dashboard_cron_email_block'), [0]))
      ->set('cron_email', $form_state->getValue('cron_email'))
      ->set('user_dashboard_cron_email_subject', $form_state->getValue('user_dashboard_cron_email_subject'))
      ->set('cron_hour', $form_state->getValue('cron_hour'))
      ->set('gridstack', $form_state->getValue('gridstack'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
