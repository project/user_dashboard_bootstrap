<?php

namespace Drupal\user_dashboard_bootstrap\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user_dashboard_bootstrap\UserDashboardBlocks;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Set show block dashboard by lock.
 */
class UserDashboardLock extends ConfigFormBase {

  /**
   * Name of the form.
   *
   * @var string
   */
  public static string $formName = 'user_dashboard_bootstrap.lock';

  /**
   * Roles list.
   *
   * @var array
   */
  protected mixed $roles;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface|null $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\user_dashboard_bootstrap\UserDashboardBlocks $userDashboardBlocks
   *   The service of userdashboard.
   * @param \Drupal\Core\Database\Connection $database
   *   Service database.
   */
  public function __construct(ConfigFactoryInterface $config_factory, protected TypedConfigManagerInterface $typedConfigManager, protected EntityTypeManagerInterface $entityTypeManager, protected UserDashboardBlocks $userDashboardBlocks, protected Connection $database) {
    parent::__construct($config_factory, $typedConfigManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('user_dashboard_bootstrap.blocks'),
      $container->get('database'),
    );
  }

  /**
   * Configuration name.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return [self::$formName];
  }

  /**
   * Form ID.
   *
   * @inheritDoc
   */
  public function getFormId() {
    return 'user_dashboard_lock_form';
  }

  /**
   * Build form.
   *
   * @inheritDoc
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::$formName);
    $valuesBlock = $config->get('lock_block');
    $blocks = $this->userDashboardBlocks->listBlocks();

    $form['link'] = [
      '#type' => 'link',
      '#title' => $this->t('Back to configuration'),
      '#url' => Url::fromRoute('user_dashboard_bootstrap.settings'),
      '#attributes' => ['class' => ['btn', 'btn-success']],
    ];
    $form['layout'] = [
      '#type' => 'link',
      '#title' => $this->t('Set user dashboard layout by role'),
      '#url' => Url::fromRoute('user_dashboard_bootstrap.user_role_layout'),
      '#attributes' => ['class' => ['btn', 'btn-warning', 'ms-2']],
    ];

    $form['lock_block'] = [
      '#title' => 'Lock block',
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#options' => $blocks,
      '#default_value' => !empty($valuesBlock) ? $valuesBlock : [],
      '#prefix' => '<div class="user-dashboard-checkbox layout-column col-12 mb-4">',
      '#suffix' => '</div>',
    ];

    $form['#attached']['library'] = ['user_dashboard_bootstrap/user_dashboard'];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Submit form.
   *
   * @inheritDoc
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    foreach ($form_state->getValues() as $key => $value) {

      if (stripos($key, 'lock_block') !== 0) {
        continue;
      }

      $value = array_diff($value, [0]);
      $config = $this->config(self::$formName);
      $config->clear($key);
      if (!empty($value)) {
        $config->set($key, $value);
      }
      $config->save();
    }
    parent::submitForm($form, $form_state);
  }

}
