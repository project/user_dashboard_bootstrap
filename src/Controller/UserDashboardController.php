<?php

namespace Drupal\user_dashboard_bootstrap\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\user_dashboard_bootstrap\UserDashboardBlocks;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for User dashboard Bootstrap routes.
 */
class UserDashboardController extends ControllerBase {

  /**
   * Constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request.
   * @param \Drupal\user_dashboard_bootstrap\UserDashboardBlocks $userDashboardBlocks
   *   User dashboard blocks.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Service render.
   * @param \Drupal\Core\Database\Connection $database
   *   Service database.
   */
  public function __construct(protected RequestStack $requestStack, protected UserDashboardBlocks $userDashboardBlocks, protected RendererInterface $renderer, protected Connection $database) {
    $this->currentUser();
    $this->entityTypeManager();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('request_stack'),
      $container->get('user_dashboard_bootstrap.blocks'),
      $container->get('renderer'),
      $container->get('database'),
    );
  }

  /**
   * Page user dashboard.
   *
   * @inheritDoc
   */
  public function dashboard($user) {
    $js_settings = [
      'dashboard' => [
        'drawer' => Url::fromRoute('user_dashboard_bootstrap.drawer', ['user' => $user])
          ->toString(),
        'blockContent' => Url::fromRoute('user_dashboard_bootstrap.block_content', ['user' => $user])
          ->toString(),
        'updatePath' => Url::fromRoute('user_dashboard_bootstrap.update', ['user' => $user])
          ->toString(),
        'customize' => Url::fromRoute('user_dashboard_bootstrap.customize', ['user' => $user])
          ->toString(),
        'dashboard' => Url::fromRoute('user_dashboard_bootstrap.id_dashboard', ['user' => $user])
          ->toString(),
        'emptyBlockText' => $this->t('empty'),
        'emptyRegionTextInactive' => $this->t('This dashboard region is empty. Click <em>Customize dashboard</em> to add blocks to it.'),
        'emptyRegionTextActive' => $this->t('DRAG HERE'),
      ],
    ];
    $regions = $this->userDashboardBlocks->getUserdashboardRegionsBlocks($user);
    $theme = 'user_dashboard_page';
    $library = 'user_dashboard';
    $config = $this->config('user_dashboard_bootstrap.settings');
    if ($config->get('gridstack')) {
      $theme = 'user_dashboard_gridstack';
      $library = 'user_dashboard_gridstack';
      $regionGridStack["main"]["user_dashboard_main"] = $regions["main"]["user_dashboard_main"];
      $regions = $regionGridStack;
    }
    $build = [
      '#theme' => $theme,
      '#regions' => $regions,
      '#message' => $this->t('To customize the dashboard page, move blocks to the dashboard regions on the <a href="@dashboard">Dashboard administration page</a>, or enable JavaScript on this page to use the drag-and-drop interface.', [
        '@dashboard' => Url::fromRoute('user_dashboard_bootstrap.set_default')
          ->toString(),
      ]),
      '#configuration' => $this->t('Drag and drop these blocks to the columns below. Changes are automatically saved. More options are available on the UserDashboard <a href="@dashboard">configuration page</a>.', [
        '@dashboard' => Url::fromRoute('user_dashboard_bootstrap.settings')
          ->toString(),
      ]),
    ];
    $hasPermission = $this->currentUser->hasPermission('customize user_dashboard blocks') ||
      $this->currentUser->hasPermission('set default user_dashboard blocks');
    $userRoles = $this->currentUser->getRoles();
    if (in_array('administrator', $userRoles)) {
      $hasPermission = TRUE;
    }
    if ($hasPermission) {
      $build['#customize'] = $this->t('Customize dashboard');
      $build['#attached'] = [
        'library' => ['user_dashboard_bootstrap/' . $library],
        'drupalSettings' => $js_settings,
      ];
    }

    return $build;
  }

  /**
   * Customize.
   *
   * @inheritDoc
   */
  public function customize($user) {
    $blocks = $this->getBlocksAvailable($user);
    // Remove blocks selected.
    $blockSelected = $this->userDashboardBlocks->getUserdashboardBlocks($user);
    foreach ($blocks as $delta => $block) {
      if (!empty($blockSelected[$delta])) {
        unset($blocks[$delta]);
      }
    }
    $config = $this->config('user_dashboard_bootstrap.settings');
    $theme = $config->get('gridstack') ? 'user_dashboard_gridstack_blocks' : 'user_dashboard_disabled_blocks';
    $build = [
      '#theme' => $theme,
      '#blocks' => $blocks,
    ];
    return new Response($this->renderer->render($build));
  }

  /**
   * Drawer.
   *
   * @inheritDoc
   */
  public function drawer(Request $request, $user) {
    $region = $request->request->get('region');
    $blockId = $request->request->get('blockId');
    $bid = 0;
    if (!empty($region) && !empty($blockId)) {
      $query = $this->database->select('user_dashboard_block', 'b')
        ->fields('b', ['bid'])
        ->condition('delta', $blockId)
        ->condition('uid', $user);
      $bid = $query->execute()->fetchField();
      if (empty($bid)) {
        $bid = $this->database->insert('user_dashboard_block')
          ->fields(['delta', 'status', 'weight', 'region', 'uid'])
          ->values([$blockId, 1, 0, $region, $user])
          ->execute();
      }
    }
    elseif (!empty($grid = $request->request->get('gridStack'))) {
      $gridStacks = json_decode($grid, TRUE);
      $query = $this->database->select('user_dashboard_block', 'b')
        ->fields('b', ['bid', 'delta', 'position'])
        ->condition('uid', $user);
      $blocks = $query->execute()->fetchAllAssoc('delta');
      $region = 'user_dashboard_main';
      foreach ($gridStacks as $weight => $gridStack) {
        $blockId = $gridStack['id'];
        $position = json_encode($gridStack);
        if (empty($blocks[$blockId])) {
          $bid = $this->database->insert('user_dashboard_block')
            ->fields(['delta', 'status', 'weight', 'region', 'uid', 'position'])
            ->values([$blockId, TRUE, $weight, $region, $user, $position])
            ->execute();
        }
        elseif ($blocks[$blockId]->postion != $position) {
          $query = $this->database->update('user_dashboard_block')
            ->fields(['position' => $position, 'region' => $region]);
          $query->condition('bid', $blocks[$blockId]->bid);
          $query->execute();
        }
      }
    }

    return new JsonResponse([
      'data' => ['id' => $bid],
      'method' => 'GET',
      'status' => 200,
    ]);
  }

  /**
   * Update status user dashboard.
   *
   * @inheritDoc
   */
  public function update(Request $request, $user) {
    $bid = $request->request->get('bid');
    $action = $request->request->get('action');
    $result = FALSE;
    if ($action == 'delete') {
      $query = $this->database->delete('user_dashboard_block');
      $query->condition('bid', $bid);
      $query->condition('uid', $user);
      $result = $query->execute();
    }
    if ($action == 'collapse') {
      $status = $request->request->get('status');
      $status = empty($status) ? 2 : 0;
      $query = $this->database->update('user_dashboard_block')
        ->fields(['custom' => $status]);
      $query->condition('bid', $bid);
      $query->condition('uid', $user);
      $result = $query->execute();
    }
    if ($action == 'weight') {
      $bids = $request->request->get('bids');
      foreach (explode(',', $bids) as $weight => $bid) {
        $query = $this->database->update('user_dashboard_block')
          ->fields(['weight' => $weight]);
        $query->condition('bid', $bid);
        $query->condition('uid', $user);
        $result = $query->execute();
      }
    }
    if ($action == 'move' && is_numeric($bid)) {
      $requestRegion = explode('--', $request->request->get('region'));
      $region = end($requestRegion);
      $query = $this->database->update('user_dashboard_block')
        ->fields(['region' => $region]);
      $query->condition('bid', $bid);
      $query->condition('uid', $user);
      $result = $query->execute();
    }
    return new JsonResponse([
      'data' => ['result' => $result],
      'method' => 'GET',
      'status' => 200,
    ]);
  }

  /**
   * Show block content.
   *
   * @inheritDoc
   */
  public function showBlockContent($user, $delta = '') {
    $build = $this->userDashboardBlocks->renderUserdashboardBlock($user, $delta);
    return new Response($this->renderer->render($build));
  }

  /**
   * Get all blocks available.
   *
   * @inheritDoc
   */
  private function getBlocksAvailable($user) {
    $settings = $this->config('user_dashboard.settings');
    $blocksAvailable = $settings->get('user_dashboard_available_blocks');
    $definitions = $this->userDashboardBlocks->getDefinitions();
    if (empty($blocksAvailable)) {
      return $definitions;
    }
    else {
      foreach ($blocksAvailable as $index => $available) {
        $blocksAvailable[$index] = $definitions[$available];
      }
    }
    return $blocksAvailable;
  }

  /**
   * Redirect to user dashboard.
   *
   * @inheritDoc
   */
  public function goto() {
    return $this->redirect('user_dashboard_bootstrap.id_dashboard', [
      'user' => $this->currentUser->id(),
    ]);
  }

}
